<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Hash;

class LoginController extends Controller
{
   
    public function admin_login(Request $request)
    {
    	$this->validate($request,[
    			'email' => 'required ',
    			'password' => 'required'
    		]);
    	$email= $request->input('email');
    	$password =$request->input('password');
       
        $admin = DB::table('admin_auth')
        ->select('*')
        ->where('email', $email);

        if($admin->count()>0) {
            $admin = $admin->first();
            if(Hash::check($password, $admin->password)) {
                 //User has provided valid credentials :)
                $v_status=$admin->verified_status;
                if($v_status==1)
                {
                    session()->put('auth_admin_id',$admin->id);
                    session()->put('auth_admin_email',$admin->email);
                    return redirect('/admin/dashboard');
                }else{
                    return back()->with('info',' Please verify your email');
                }
                
            }
            else{
                return back()->with('info',' Password Not Match. try Again');
            }
        }else{
            return back()->with('info',' Email not registered.. Contact Administration');
        }

    }

    

}
