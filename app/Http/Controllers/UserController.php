<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use App\UserModel;
//use Auth;
//use Session;
use Hash;

class UserController extends Controller
{
    private $base_url;

    public function __construct()
    {
        //blockio init
        $this->base_url = 'http://127.0.0.1:8000';
      
    }

    //
    public function insert_users(Request $request)
    {
    	//echo $request->input('email');
    	$this->validate($request,[
    			'email' => 'required | unique:users',
    			'password' => 'required| min:6'
    		]);
    	
            $email=$request->input('email');
            $a_token=md5(time());

            $user_reg = New UserModel;
            $user_reg->email = $email;
            $user_reg->password = Hash::make($request->input('password'));
            $user_reg->access_token = $a_token;
            $user_reg->verified_status = 0;
            $user_reg->created_at =  NOW();
            $user_reg->updated_at = NOW();
            $result=$user_reg->save();
            if($result)
            {
                $id = $user_reg->id;
                $user_data=array(
                        'email' => $email,
                        'subject' => 'Verify Your Mail',
                        'url' => $this->base_url.'/verify/'.$id.'/'.$a_token
                    );
                    Mail::to($email)->send(new SendMail($user_data));

                    return redirect('/verify')->with('info','Registration Successfull');
            }else{
                return redirect('/register')->with('info','Failed to register');
            }
    	
    }

    public function verify_mail($id, $token)
    {
    	$user_id=$id;
       // Retrieve a model by its primary key...
        $usermodel = UserModel::find($user_id);
        // Retrieve the first model matching the query constraints...
        $usermodel = UserModel::where('access_token', $token)->first();
        if(!empty($usermodel))
        {
            $new_token=md5(time());
            $verified=1;
            $data=array(
                'access_token' => $new_token,
                'email_verified_at' => NOW(),
                'verified_status' => 1,
                'updated_at' => NOW()
            );
            $update=UserModel::where('id', $user_id)->update($data);
            if($update)
            {
                session()->put('auth_user_id',$user_id);
                //$uu=session()->get('user_auth_id');

                return redirect('registration-step-1');
            }
            else{
            return redirect('/verify')->with('invalid_token','Failed to verify please try again');
           }
        }else{
            return redirect('/verify')->with('invalid_token','Your accesstoken is expired');
        }
        
    	
    }

    public function login_user(Request $request)
    {
        $this->validate($request,[
                'email' => 'required ',
                'password' => 'required'
            ]);
        $email= $request->input('email');
        $password =$request->input('password');
       
        $user = UserModel::where('email', $email)
        ->select('email', 'password','id','verified_status');
        

        if($user->count()>0) {
            $user = $user->first();
            if(Hash::check($password, $user->password)) {
                 //User has provided valid credentials :)
                $v_status=$user->verified_status;
                if($v_status==1)
                {
                    session()->put('auth_user_id',$user->id);
                    session()->put('auth_email',$user->email);
                    if($user->name=='' || $user->user_type=='')
                    {
                        return redirect('registration-step-1');
                    }else{
                        return redirect('/success-login')->with('info','Success');
                    }
                    
                }else{
                    return back()->with('info',' Please verify your email');
                }
                
            }
            else{
                return back()->with('info',' Password Not Matched. try Again');
            }
        }else{
            return back()->with('info',' Email not registered.. Please create an account');
        }

    }

    public function reset_password($id, $token)
    {
        $user_id=$id;
        $users=UserModel::where('id', $user_id)
                            ->where('access_token',$token )
                             ->first();
       if(!empty($users))
        {
              return view('reset-password')->with(['user_id'=>$user_id,'token'=>$token]);
        }else{
                return view('reset-password')->with('err_msg','Oop\'s token expired.. please try again');
        } 
        
    }
    public function insert_reg_step1(Request $request)
    {
    	$this->validate($request,[
    			'name' => 'required ',
    			'surname1' => 'required',
    			'dni' =>'required',
    			'user_type' => 'required',
                'country' => 'required'
    		]);

    	$user_id=session()->get('auth_user_id');
    	$user_type=$request->input('user_type');

    	$img_name='';
        if ($request->hasFile('profile_pic')) {
            $img = $request->file('profile_pic');
            $img_name = time().'.'.$img->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $img->move($destinationPath, $img_name);
           
        }

    	$data=array(
    		'name' => $request->input('name'),
    		'surname1' => $request->input('surname1'),
    		'surname2' => $request->input('surname2'),
    		'dni' => $request->input('dni'),
    		'nickname' => $request->input('nickname'),
    		'profile_pic' => $img_name,
            'country' => $request->input('country'),
            'region' => $request->input('region'),
            'city' => $request->input('city'),
    		'user_type' => $request->input('user_type'),
            'updated_at' => NOW()
    	);
        $update = UserModel::where('id', $user_id)->update($data);
    	if($update)
    	{
    		if($user_type=='owner')
    		{

    			return redirect('/moter-cycle-registration');
    		}
    		elseif($user_type=='buyer')
    		{
    			return redirect('/');
    		}else{
    			return redirect('/registration-step-1')->with('failed... to manage use type');
    		}

    	}else{
    		return redirect('/registration-step-1')->with('failed... Please try again');
    	}

    }

    

    public function forgot_password(Request $request)
    {
        $this->validate($request,[
                'email' => 'required'
        ]);
        $email=$request->input('email');
        $user = DB::table('users')
        ->select('email', 'id','verified_status','access_token')
        ->where('email', $email);

        if($user->count()) {
            $user = $user->first();
                 //User has provided valid credentials :)
                $v_status=$user->verified_status;
                if($v_status==1)
                {
                    $access_t = $user->access_token;
                    $user_id = $user->id;

                    $user_data=array(
                        'email' => $email,
                        'subject' => 'Reset Your Password',
                        'url' => $this->base_url.'/reset_password/'.$user_id.'/'.$access_t
                    );
                  Mail::to($email)->send(new SendMail($user_data));
                    

                    return redirect('/verify')->with('invalid_token','Check your Mail id and generate Password');
                }else{
                    return back()->with('info',' Please verify your email');
                }
                
            
        }else{
            return back()->with('info',' Email not exist in our database.. ');
        }

    }

    public function generate_new_password(Request $request)
    {
        $this->validate($request,[
                'user_id' => 'required',
                'token' => 'required',
                'new_password' => 'min:4|required_with:confirm_password|same:confirm_password',
                'confirm_password' => 'required | min:4'
        ]);
        $user_id = $request->input('user_id');
        $token = $request->input('token');
        $pass = $request->input('new_password');
        $users = UserModel::where(['id'=> $user_id,'access_token'=>$token])->get();
        if(sizeof($users) > 0)
        {
            $data=array(
                'password' => Hash::make($pass),
                'access_token' => md5(time()),
                'updated_at' =>NOW()
            );
            $update=UserModel::where('id',$user_id)->update($data);
            if($update)
            {
                return back()->with('info','Successfully password changed');

            }else{
                return back()->with('info','failed to update.. Please try again');
            }

        }else{
            return back()->with('info','invalid request token');
        }
    }

    public function step_1_reg_insert(Request $request)
    {
        $this->validate($request, [

                'filename' => 'required',
                'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'brand_id' => 'required',
                'model_id' => 'required',
                'frame_no' => 'required',
                'frame_img' => 'required',
                'purchase_date' => 'required',
                'new_or_used' =>  'required'


        ]);
        $frame_img='';
        if ($request->hasFile('frame_img')) {
            $f_img = $request->file('frame_img');
            $frame_img = time().'f.'.$f_img->getClientOriginalExtension();
            $destinationPath = public_path('/images/products/');
            $f_img->move($destinationPath, $frame_img);
           
        }
        $mileage_img='';
        if ($request->hasFile('mileage_img')) {
            $m_img = $request->file('mileage_img');
            $mileage_img = time().'m.'.$m_img->getClientOriginalExtension();
            $destinationPath = public_path('/images/products/');
            $m_img->move($destinationPath, $mileage_img);
           
        }
        $img_name='';
        if($request->hasfile('filename'))
         {
            $i=1;
            foreach($request->file('filename') as $image)
            {
                $i++;
                $img_name = time().$i.'.'.$image->getClientOriginalExtension();
                //$name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $img_name);  
                $img_data[] = $img_name;  
            }
            $bike_imgs = implode(",",$img_data);
           
         }
         $data = array(
            'user_id' => session()->get('auth_user_id'),
            'brand_id' => $request->input('brand_id'),
            'model_id' => $request->input('model_id'),
            'frame_no' => $request->input('frame_no'),
            'frame_img' => $frame_img,
            'purchase_date' => $request->input('purchase_date'),
            'new_or_used' => $request->input('new_or_used'),
            'previous_owner_no' => $request->input('previous_owner_no'),
            'bike_imgs' => $bike_imgs,
            'mileage' => $request->input('mileage'),
            'mileage_img' => $mileage_img

         );

         $result=DB::table('products')->insert($data);
         if($result)
         {
            return back()->with('info',' Success');

         }else{
                return back()->with('info',' fail');
         }
    }

    public function motor_cycle_registration()
    {
        $brands = DB::table("motorcycle_brand")->pluck("brand_name","id");
        return view('moter-cycle-registration',compact('brands'));
    }

    public function getModelList(Request $request)
    {
        $models = DB::table("motorcycle_model")
            ->where("brand_id",$request->brand_id)
            ->pluck("model_name","id");
            return response()->json($models);
    }

    public function logout()
    {
        Session::flush();
        return redirect('/')->with('info','Sucessfully  Logged Out');
    }

    

   
}
